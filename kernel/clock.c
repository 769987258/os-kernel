#include "type.h"
#include "const.h"
#include "protect.h"
#include "proto.h"
#include "string.h"
#include "proc.h"
#include "global.h"

PUBLIC void clock_handler(int irq)
{
	ticks++;
	p_proc_ready->ticks--;
	if(k_reenter != 0){
		return;
	}
	
	if(p_proc_ready->ticks > 0)
	{
		return;
	}
	//每次时钟中断进行一次进程调度
	schedule();
}

PUBLIC void milli_delay(int milli_sec)
{
	int t = get_ticks();
	//ticks之间的间隔时间是（1000/Hz）ms，故delta t个ticks相当于（delta t * 1000/Hz）ms
	while((get_ticks() - t) * 1000/ HZ < milli_sec){}
}

