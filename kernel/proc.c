#include "type.h"
#include "const.h"
#include "protect.h"
#include "proto.h"
#include "string.h"
#include "proc.h"
#include "global.h"


PUBLIC int sys_get_ticks()
{
	return ticks;
}


//进程调度
PUBLIC void schedule()
{
	PROCESS* p;
	int	greatest_ticks = 0;
	while(!greatest_ticks) {
		//选出ticks最大的进行调度
		for(p = proc_table; p < proc_table+NR_TASKS; p++)
		{
			if(p->ticks > greatest_ticks) {
				greatest_ticks = p->ticks;
				p_proc_ready = p;
			}
		}
		//若所有进程ticks均为0,则将其ticks全部置为priority
		/*if(!greatest_ticks)
		{
			for(p = proc_table; p < proc_table+NR_TASKS; p++)
			{
				p->ticks = p->priority;
			}
		}*/
	}
}
