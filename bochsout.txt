00000000000i[      ] Bochs x86 Emulator 2.6.11
00000000000i[      ]   Built from SVN snapshot on January 5, 2020
00000000000i[      ]   Timestamp: Sun Jan  5 08:36:00 CET 2020
00000000000i[      ] System configuration
00000000000i[      ]   processors: 1 (cores=1, HT threads=1)
00000000000i[      ]   A20 line support: yes
00000000000i[      ]   load configurable MSRs from file "msrs.def"
00000000000i[      ] IPS is set to 50000000
00000000000i[      ] CPU configuration
00000000000i[      ]   SMP support: no
00000000000i[      ]   Using pre-defined CPU configuration: core_duo_t2400_yonah
00000000000i[      ] Optimization configuration
00000000000i[      ]   RepeatSpeedups support: no
00000000000i[      ]   Fast function calls: no
00000000000i[      ]   Handlers Chaining speedups: no
00000000000i[      ] Devices configuration
00000000000i[      ]   PCI support: i440FX i430FX i440BX
00000000000i[      ]   Networking: no
00000000000i[      ]   Sound support: no
00000000000i[      ]   USB support: no
00000000000i[      ]   VGA extension support: vbe
00000000000i[MEM0  ] allocated memory at 0x7fcffa4d5010. after alignment, vector=0x7fcffa4d6000
00000000000i[MEM0  ] 32.00MB
00000000000i[MEM0  ] mem block size = 0x00020000, blocks=256
00000000000i[MEM0  ] rom at 0xfffe0000/131072 ('/usr/local/share/bochs/BIOS-bochs-latest')
00000000000i[PLUGIN] init_dev of 'pci' plugin device by virtual method
00000000000i[DEV   ] i440FX PMC present at device 0, function 0
00000000000i[PLUGIN] init_dev of 'pci2isa' plugin device by virtual method
00000000000i[DEV   ] PIIX3 PCI-to-ISA bridge present at device 1, function 0
00000000000i[PLUGIN] init_dev of 'cmos' plugin device by virtual method
00000000000i[CMOS  ] Using local time for initial clock
00000000000i[CMOS  ] Setting initial clock to: Sun Mar 14 10:47:46 2021 (time0=1615690066)
00000000000i[PLUGIN] init_dev of 'dma' plugin device by virtual method
00000000000i[DMA   ] channel 4 used by cascade
00000000000i[PLUGIN] init_dev of 'pic' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'pit' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'vga' plugin device by virtual method
00000000000i[MEM0  ] Register memory access handlers: 0x0000000a0000 - 0x0000000bffff
00000000000i[VGA   ] interval=200000, mode=realtime
00000000000i[VGA   ] VSYNC using standard mode
00000000000i[MEM0  ] Register memory access handlers: 0x0000e0000000 - 0x0000e0ffffff
00000000000i[BXVGA ] VBE Bochs Display Extension Enabled
00000000000i[XGUI  ] test_alloc_colors: 16 colors available out of 16 colors tried
00000000000i[XGUI  ] font 8 wide x 16 high, display depth = 24
00000000000i[KEYMAP] Loading keymap from '/usr/local/share/bochs/keymaps/x11-pc-us.map'
00000000000i[KEYMAP] Loaded 168 symbols
00000000000i[MEM0  ] rom at 0xc0000/41984 ('/usr/local/share/bochs/VGABIOS-lgpl-latest')
00000000000i[PLUGIN] init_dev of 'floppy' plugin device by virtual method
00000000000i[DMA   ] channel 2 used by Floppy Drive
00000000000i[FLOPPY] fd0: 'disk.img' ro=0, h=2,t=80,spt=18
00000000000i[FLOPPY] Using boot sequence disk, none, none
00000000000i[FLOPPY] Floppy boot signature check is enabled
00000000000i[PLUGIN] init_dev of 'acpi' plugin device by virtual method
00000000000i[DEV   ] ACPI Controller present at device 1, function 3
00000000000i[PLUGIN] init_dev of 'hpet' plugin device by virtual method
00000000000i[HPET  ] initializing HPET
00000000000i[MEM0  ] Register memory access handlers: 0x0000fed00000 - 0x0000fed003ff
00000000000i[PLUGIN] init_dev of 'ioapic' plugin device by virtual method
00000000000i[IOAPIC] initializing I/O APIC
00000000000i[MEM0  ] Register memory access handlers: 0x0000fec00000 - 0x0000fec00fff
00000000000i[IOAPIC] IOAPIC enabled (base address = 0xfec00000)
00000000000i[PLUGIN] init_dev of 'keyboard' plugin device by virtual method
00000000000i[KBD   ] will paste characters every 400 keyboard ticks
00000000000i[PLUGIN] init_dev of 'harddrv' plugin device by virtual method
00000000000i[HD    ] HD on ata0-0: 'disk.img', 'flat' mode
00000000000i[IMG   ] hd_size: 1474560
00000000000i[HD    ] ata0-0: autodetect geometry: CHS=2/16/63 (sector size=512)
00000000000i[HD    ] ata0-0: extra data outside of CHS address range
00000000000i[HD    ] translation on ata0-0 set to 'none'
00000000000i[PLUGIN] init_dev of 'pci_ide' plugin device by virtual method
00000000000i[DEV   ] PIIX3 PCI IDE controller present at device 1, function 1
00000000000i[PLUGIN] init_dev of 'unmapped' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'biosdev' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'speaker' plugin device by virtual method
00000000000e[PCSPK ] Failed to open /dev/console: 权限不够
00000000000e[PCSPK ] Deactivating beep on console
00000000000i[PLUGIN] init_dev of 'extfpuirq' plugin device by virtual method
00000000000i[PLUGIN] init_dev of 'parallel' plugin device by virtual method
00000000000i[PAR   ] parallel port 1 at 0x0378 irq 7
00000000000i[PLUGIN] init_dev of 'serial' plugin device by virtual method
00000000000i[SER   ] com1 at 0x03f8 irq 4 (mode: null)
00000000000i[PLUGIN] init_dev of 'iodebug' plugin device by virtual method
00000000000i[PLUGIN] register state of 'pci' plugin device by virtual method
00000000000i[PLUGIN] register state of 'pci2isa' plugin device by virtual method
00000000000i[PLUGIN] register state of 'cmos' plugin device by virtual method
00000000000i[PLUGIN] register state of 'dma' plugin device by virtual method
00000000000i[PLUGIN] register state of 'pic' plugin device by virtual method
00000000000i[PLUGIN] register state of 'pit' plugin device by virtual method
00000000000i[PLUGIN] register state of 'vga' plugin device by virtual method
00000000000i[PLUGIN] register state of 'floppy' plugin device by virtual method
00000000000i[PLUGIN] register state of 'unmapped' plugin device by virtual method
00000000000i[PLUGIN] register state of 'biosdev' plugin device by virtual method
00000000000i[PLUGIN] register state of 'speaker' plugin device by virtual method
00000000000i[PLUGIN] register state of 'extfpuirq' plugin device by virtual method
00000000000i[PLUGIN] register state of 'parallel' plugin device by virtual method
00000000000i[PLUGIN] register state of 'serial' plugin device by virtual method
00000000000i[PLUGIN] register state of 'iodebug' plugin device by virtual method
00000000000i[PLUGIN] register state of 'acpi' plugin device by virtual method
00000000000i[PLUGIN] register state of 'hpet' plugin device by virtual method
00000000000i[PLUGIN] register state of 'ioapic' plugin device by virtual method
00000000000i[PLUGIN] register state of 'keyboard' plugin device by virtual method
00000000000i[PLUGIN] register state of 'harddrv' plugin device by virtual method
00000000000i[PLUGIN] register state of 'pci_ide' plugin device by virtual method
00000000000i[SYS   ] bx_pc_system_c::Reset(HARDWARE) called
00000000000i[CPU0  ] cpu hardware reset
00000000000i[APIC0 ] allocate APIC id=0 (MMIO enabled) to 0x0000fee00000
00000000000i[CPU0  ] CPUID[0x00000000]: 0000000a 756e6547 6c65746e 49656e69
00000000000i[CPU0  ] CPUID[0x00000001]: 000006ec 00010800 0000c189 9febfbff
00000000000i[CPU0  ] CPUID[0x00000002]: 02b3b001 000000f0 00000000 2c04307d
00000000000i[CPU0  ] CPUID[0x00000003]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x00000004]: 04000121 01c0003f 0000003f 00000001
00000000000i[CPU0  ] CPUID[0x00000005]: 00000040 00000040 00000003 00022220
00000000000i[CPU0  ] CPUID[0x00000006]: 00000001 00000002 00000001 00000000
00000000000i[CPU0  ] CPUID[0x00000007]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x00000008]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x00000009]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] WARNING: Architectural Performance Monitoring is not implemented
00000000000i[CPU0  ] CPUID[0x0000000a]: 07280201 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x80000000]: 80000008 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x80000001]: 00000000 00000000 00000000 00100000
00000000000i[CPU0  ] CPUID[0x80000002]: 65746e49 2952286c 726f4320 4d542865
00000000000i[CPU0  ] CPUID[0x80000003]: 75442029 5043206f 20202055 54202020
00000000000i[CPU0  ] CPUID[0x80000004]: 30303432 20402020 33382e31 007a4847
00000000000i[CPU0  ] CPUID[0x80000005]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x80000006]: 00000000 00000000 08006040 00000000
00000000000i[CPU0  ] CPUID[0x80000007]: 00000000 00000000 00000000 00000000
00000000000i[CPU0  ] CPUID[0x80000008]: 00002028 00000000 00000000 00000000
00000000000i[CPU0  ] CPU Features supported:
00000000000i[CPU0  ] 		x87
00000000000i[CPU0  ] 		486ni
00000000000i[CPU0  ] 		pentium_ni
00000000000i[CPU0  ] 		p6ni
00000000000i[CPU0  ] 		mmx
00000000000i[CPU0  ] 		debugext
00000000000i[CPU0  ] 		vme
00000000000i[CPU0  ] 		pse
00000000000i[CPU0  ] 		pae
00000000000i[CPU0  ] 		pge
00000000000i[CPU0  ] 		pse36
00000000000i[CPU0  ] 		mtrr
00000000000i[CPU0  ] 		pat
00000000000i[CPU0  ] 		sysenter_sysexit
00000000000i[CPU0  ] 		clflush
00000000000i[CPU0  ] 		sse
00000000000i[CPU0  ] 		sse2
00000000000i[CPU0  ] 		sse3
00000000000i[CPU0  ] 		mwait
00000000000i[CPU0  ] 		nx
00000000000i[CPU0  ] 		xapic
00000000000i[PLUGIN] reset of 'pci' plugin device by virtual method
00000000000i[PLUGIN] reset of 'pci2isa' plugin device by virtual method
00000000000i[PLUGIN] reset of 'cmos' plugin device by virtual method
00000000000i[PLUGIN] reset of 'dma' plugin device by virtual method
00000000000i[PLUGIN] reset of 'pic' plugin device by virtual method
00000000000i[PLUGIN] reset of 'pit' plugin device by virtual method
00000000000i[PLUGIN] reset of 'vga' plugin device by virtual method
00000000000i[PLUGIN] reset of 'floppy' plugin device by virtual method
00000000000i[PLUGIN] reset of 'acpi' plugin device by virtual method
00000000000i[PLUGIN] reset of 'hpet' plugin device by virtual method
00000000000i[PLUGIN] reset of 'ioapic' plugin device by virtual method
00000000000i[PLUGIN] reset of 'keyboard' plugin device by virtual method
00000000000i[PLUGIN] reset of 'harddrv' plugin device by virtual method
00000000000i[PLUGIN] reset of 'pci_ide' plugin device by virtual method
00000000000i[PLUGIN] reset of 'unmapped' plugin device by virtual method
00000000000i[PLUGIN] reset of 'biosdev' plugin device by virtual method
00000000000i[PLUGIN] reset of 'speaker' plugin device by virtual method
00000000000i[PLUGIN] reset of 'extfpuirq' plugin device by virtual method
00000000000i[PLUGIN] reset of 'parallel' plugin device by virtual method
00000000000i[PLUGIN] reset of 'serial' plugin device by virtual method
00000000000i[PLUGIN] reset of 'iodebug' plugin device by virtual method
00000000000i[      ] set SIGINT handler to bx_debug_ctrlc_handler
00000004662i[BIOS  ] $Revision: 13752 $ $Date: 2019-12-30 14:16:18 +0100 (Mon, 30 Dec 2019) $
00000375065i[KBD   ] reset-disable command received
00000400832i[BIOS  ] Starting rombios32
00000401270i[BIOS  ] Shutdown flag 0
00000401860i[BIOS  ] ram_size=0x02000000
00000402282i[BIOS  ] ram_end=32MB
00000898188i[BIOS  ] Found 1 cpu(s)
00000911840i[BIOS  ] bios_table_addr: 0x000f9db8 end=0x000fcc00
00000911880d[PCI   ] read  PCI register 0x00 value 0x8086 (len=2)
00000911886d[PCI   ] read  PCI register 0x02 value 0x1237 (len=2)
00000911911d[PCI   ] read  PCI register 0x00 value 0x8086 (len=2)
00000911917d[PCI   ] read  PCI register 0x02 value 0x1237 (len=2)
00000911957d[PCI   ] read  PCI register 0x59 value 0x00 (len=1)
00000911966d[PCI   ] write PCI register 0x59 value 0x00 (len=1)
00001239661d[PCI   ] write PCI register 0x59 value 0x30 (len=1)
00001239661i[PCI   ] i440FX PMC write to PAM register 59 (TLB Flush)
00001567591i[P2ISA ] PCI IRQ routing: PIRQA# set to 0x0b
00001567610i[P2ISA ] PCI IRQ routing: PIRQB# set to 0x09
00001567629i[P2ISA ] PCI IRQ routing: PIRQC# set to 0x0b
00001567648i[P2ISA ] PCI IRQ routing: PIRQD# set to 0x09
00001567658i[P2ISA ] write: ELCR2 = 0x0a
00001568428i[BIOS  ] PIIX3/PIIX4 init: elcr=00 0a
00001580150d[PCI   ] read  PCI register 0x00 value 0x8086 (len=2)
00001580156d[PCI   ] read  PCI register 0x02 value 0x1237 (len=2)
00001580176d[PCI   ] read  PCI register 0x00 value 0x8086 (len=2)
00001580183d[PCI   ] read  PCI register 0x02 value 0x1237 (len=2)
00001580195d[PCI   ] read  PCI register 0x0A value 0x0600 (len=2)
00001580204d[PCI   ] read  PCI register 0x00 value 0x8086 (len=2)
00001580211d[PCI   ] read  PCI register 0x02 value 0x1237 (len=2)
00001580219d[PCI   ] read  PCI register 0x0E value 0x00 (len=1)
00001582052i[BIOS  ] PCI: bus=0 devfn=0x00: vendor_id=0x8086 device_id=0x1237 class=0x0600
00001582100d[PCI   ] read  PCI register 0x10 value 0x00000000 (len=4)
00001582128d[PCI   ] read  PCI register 0x14 value 0x00000000 (len=4)
00001582156d[PCI   ] read  PCI register 0x18 value 0x00000000 (len=4)
00001582184d[PCI   ] read  PCI register 0x1C value 0x00000000 (len=4)
00001582212d[PCI   ] read  PCI register 0x20 value 0x00000000 (len=4)
00001582240d[PCI   ] read  PCI register 0x24 value 0x00000000 (len=4)
00001582260d[PCI   ] write PCI register 0x30 value 0xFFFFFFFE (len=4)
00001582266d[PCI   ] read  PCI register 0x30 value 0x00000000 (len=4)
00001582279d[PCI   ] read  PCI register 0x3D value 0x00 (len=1)
00001582489d[PCI   ] read  PCI register 0x00 value 0x8086 (len=2)
00001582496d[PCI   ] read  PCI register 0x02 value 0x1237 (len=2)
00001584365i[BIOS  ] PCI: bus=0 devfn=0x08: vendor_id=0x8086 device_id=0x7000 class=0x0601
00001584641d[PCI   ] read  PCI register 0x00 value 0x8086 (len=2)
00001584648d[PCI   ] read  PCI register 0x02 value 0x1237 (len=2)
00001586517i[BIOS  ] PCI: bus=0 devfn=0x09: vendor_id=0x8086 device_id=0x7010 class=0x0101
00001586752i[PIDE  ] BAR #4: i/o base address = 0xc000
00001587368i[BIOS  ] region 4: 0x0000c000
00001587557d[PCI   ] read  PCI register 0x00 value 0x8086 (len=2)
00001587564d[PCI   ] read  PCI register 0x02 value 0x1237 (len=2)
00001589433i[BIOS  ] PCI: bus=0 devfn=0x0b: vendor_id=0x8086 device_id=0x7113 class=0x0680
00001589677i[ACPI  ] new IRQ line = 11
00001589691i[ACPI  ] new IRQ line = 9
00001589718i[ACPI  ] new PM base address: 0xb000
00001589732i[ACPI  ] new SM base address: 0xb100
00001589760d[PCI   ] write PCI register 0x72 value 0x4A (len=1)
00001589760i[PCI   ] setting SMRAM control register to 0x4a
00001753853i[CPU0  ] Enter to System Management Mode
00001753864i[CPU0  ] RSM: Resuming from System Management Mode
00001917885d[PCI   ] write PCI register 0x72 value 0x0A (len=1)
00001917885i[PCI   ] setting SMRAM control register to 0x0a
00001929441d[PCI   ] read  PCI register 0x00 value 0x8086 (len=2)
00001929447d[PCI   ] read  PCI register 0x02 value 0x1237 (len=2)
00001929466d[PCI   ] read  PCI register 0x0A value 0x0600 (len=2)
00001929474d[PCI   ] read  PCI register 0x30 value 0x00000000 (len=4)
00001944559i[BIOS  ] MP table addr=0x000f9e90 MPC table addr=0x000f9dc0 size=0xc8
00001946430i[BIOS  ] SMBIOS table addr=0x000f9ea0
00001948616i[BIOS  ] ACPI tables: RSDP addr=0x000f9fd0 ACPI DATA addr=0x01ff0000 size=0xff8
00001951863i[BIOS  ] Firmware waking vector 0x1ff00cc
00001954334d[PCI   ] read  PCI register 0x59 value 0x30 (len=1)
00001954343d[PCI   ] write PCI register 0x59 value 0x10 (len=1)
00001954343i[PCI   ] i440FX PMC write to PAM register 59 (TLB Flush)
00001955066i[BIOS  ] bios_table_cur_addr: 0x000f9ff4
00002083937i[VBIOS ] VGABios $Id: vgabios.c 226 2020-01-02 21:36:23Z vruppert $
00002084008i[BXVGA ] VBE known Display Interface b0c0
00002084040i[BXVGA ] VBE known Display Interface b0c5
00002086683i[VBIOS ] VBE Bios $Id: vbe.c 228 2020-01-02 23:09:02Z vruppert $
00002434196i[BIOS  ] ata0-0: PCHS=2/16/63 translation=none LCHS=2/16/63
00005881020i[BIOS  ] IDE time out
00006158450i[BIOS  ] Booting from 0000:7c00
00007651100i[XGUI  ] charmap update. Font is 9 x 16
00345299600i[      ] Ctrl-C detected in signal handler.
00345299601i[      ] dbg: Quit
00345299601i[CPU0  ] CPU is in protected mode (active)
00345299601i[CPU0  ] CS.mode = 32 bit
00345299601i[CPU0  ] SS.mode = 32 bit
00345299601i[CPU0  ] EFER   = 0x00000000
00345299601i[CPU0  ] | EAX=ffffffff  EBX=00000000  ECX=00000000  EDX=ffffffff
00345299601i[CPU0  ] | ESP=00032870  EBP=00032880  ESI=0004b6d0  EDI=0000010c
00345299601i[CPU0  ] | IOPL=1 id vip vif ac vm rf nt of df IF tf SF zf af PF cf
00345299601i[CPU0  ] | SEG sltr(index|ti|rpl)     base    limit G D
00345299601i[CPU0  ] |  CS:0008( 0001| 0|  0) 00000000 ffffffff 1 1
00345299601i[CPU0  ] |  DS:0010( 0002| 0|  0) 00000000 ffffffff 1 1
00345299601i[CPU0  ] |  SS:0010( 0002| 0|  0) 00000000 ffffffff 1 1
00345299601i[CPU0  ] |  ES:0010( 0002| 0|  0) 00000000 ffffffff 1 1
00345299601i[CPU0  ] |  FS:000d( 0001| 1|  1) 00000000 ffffffff 1 1
00345299601i[CPU0  ] |  GS:0019( 0003| 0|  1) 000b8000 0000ffff 0 0
00345299601i[CPU0  ] | EIP=000314e5 (000314e5)
00345299601i[CPU0  ] | CR0=0xe0000011 CR2=0x00000000
00345299601i[CPU0  ] | CR3=0x00200000 CR4=0x00000000
00345299601d[PCI   ] Exit
00345299601i[CMOS  ] Last time is 1615690072 (Sun Mar 14 10:47:52 2021)
00345299601i[XGUI  ] Exit
00345299601i[SIM   ] quit_sim called with exit code 0
